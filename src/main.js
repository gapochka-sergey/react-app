const ReactDOM = require('react-dom');
const React = require('react');
const MainComponent = require('./components/MainComponent.js');
const PreviewComponent = require('./components/PreviewComponent.js');

ReactDOM.render(
  <div>
    <MainComponent />
    
  </div>,
  document.getElementById('root')
);
