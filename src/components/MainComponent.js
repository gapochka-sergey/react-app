const React = require('react');
const PreviewComponent = require('./PreviewComponent.js')

const MAX_COUNT_STRIPES = 100;
const MIN_COUNT_STRIPES = 2;

require('./MainComponent.css');

var MainComponent = React.createClass ({
  getInitialState() {
    return {
      count: 8
    };
  },

  addStripe() {
    if (this.state.count > MAX_COUNT_STRIPES) {
      return false;
    }
    this.setState({ count: this.state.count + 1 });
  },

  deleteStripe() {
    if (this.state.count < MIN_COUNT_STRIPES) {
      return false;
    }
    this.setState({ count: this.state.count - 1 });
  },

  addOddStripe(stripes, number) {
    stripes.push(<div key={number}
      style={{background: "white"}}
      className="main-component__stripes"></div>);
  },

  addEvenStripe(stripes, number) {
    stripes.push(<div key={number}
      style={{background: "silver"}}
      className="main-component__stripes"></div>)
  },

  addRedStripe(stripes, number) {
    stripes.push(<div key={number}
      style={{background: "rgb(255, 99, 71)"}} className="main-component__stripes"></div>);
  },

  render() {
    const stripes = [];

    for (var i = 0; i < this.state.count; i++) {
      if (i % 2 == 0) {
        if (i == this.state.count - 2) {
          this.addRedStripe(stripes, i);
        } else {
          this.addEvenStripe(stripes, i);
        }

      } else {
        if (i == this.state.count - 2) {
          this.addRedStripe(stripes, i);
        } else {
          this.addOddStripe(stripes, i);
        }
      }
    }

    return(
      <div>
        <div className="main-component">
          <div className="main-component__stripes">
            {stripes}
          </div>
          <div className="main-component__control">
            <button onClick={this.deleteStripe} className="main-component__control-remove"> - </button>
            <button onClick={this.addStripe} className="main-component__control-add"> + </button>
          </div>
        </div>
        <PreviewComponent totalCount={this.state.count} />
      </div>
    );
  }
});

module.exports = MainComponent;
