const React = require('react');

var MainComponent = require('./MainComponent.js');

require('./PreviewComponent.css');

var PreviewComponent = React.createClass({
  getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
  },

  getOptions() {
    return {
      colors: {
        randomEvenColor:  "#"+((1<<24)*Math.random()|0).toString(16),
        randomOddColor: "#"+((1<<24)*Math.random()|0).toString(16)
      }
    }
  },

  addOddStripe(stripes, number, options) {
    console.log(options);
    stripes.push(<div key={number}
      style={{background: options.colors.randomEvenColor}} className="main-component__stripes-preview"></div>);
  },

  addEvenStripe(stripes, number, options) {
    stripes.push(<div key={number}
      style={{background: options.colors.randomOddColor}} className="main-component__stripes-preview"></div>)
  },

  addRandomRedStripe(stripes, number) {
    stripes.push(<div key={number}
      style={{background: "rgb(255, 99, 71)"}} className="main-component__stripes-preview"></div>);
  },

  renderPreview() {
    const stripes = [];
    const randomInt = this.getRandomInt(1, this.props.totalCount - 1);
    const options = this.getOptions();

    for (var i = 0; i < this.props.totalCount; i++) {
      if (randomInt == i) {
        this.addRandomRedStripe(stripes, i, options);
        continue;
      }
      if (i % 2 == 0) {
        this.addEvenStripe(stripes, i, options);
      } else {
        this.addOddStripe(stripes, i, options);
      }
    }
    return stripes;
  },

  render() {
    return(
      <div>
        <div className="lines">
          <h1>Total lines: {this.props.totalCount}</h1>
        </div>
          <div className="preview-component">
            {this.renderPreview()}
          </div>
          <div className="preview-component">
            {this.renderPreview()}
          </div>
          <div className="preview-component">
            {this.renderPreview()}
          </div>
          <div className="preview-component">
            {this.renderPreview()}
          </div>

        </div>
    )
  }
});
module.exports = PreviewComponent;
